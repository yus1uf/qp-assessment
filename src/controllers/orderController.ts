import { Request, Response } from 'express';
import GroceryItem from '../database/model/groceryItem';
import Order from '../database/model/order';
import { HTTP_CODES, MESSAGES } from '../helper/constant';
import { customResponse } from '../helper/customResponse';

export const bookItems = async (req: Request, res: Response) => {
  try {
    if (!req.user) {
      const errorResponse = customResponse({ code: HTTP_CODES.UNAUTHORIZED, message: MESSAGES.UNAUTHORIZED });
      return res.status(HTTP_CODES.UNAUTHORIZED).send(errorResponse);
    }
    const { itemIds } = req.body;

    const groceryItems = await GroceryItem.findAll({ where: { id: itemIds } });
    let price = 0;
    // Check if all selected items are available
    for (const item of groceryItems) {
      if (item.inventory === 0) {
        const errorResponse = customResponse({
          code: HTTP_CODES.BAD_REQUEST,
          message: `item '${item.name}' is out of stock`,
        });
        return res.status(HTTP_CODES.BAD_REQUEST).send(errorResponse);
      } else {
        price += item.price;
      }
    }
    const order = await Order.create({ userId: req.user?.id, totalPrice: price });
    // Update inventory and associate grocery items with the order
    for (const item of groceryItems) {
      await item.update({ inventory: item.inventory - 1 });
      order?.groceryItems?.push(item);
    }
    const orderDetails = {
      orderId: order.id,
      ammount: price,
    };
    const successResponse = customResponse({ code: HTTP_CODES.CREATED, message: MESSAGES.BOOKED, data: orderDetails });

    return res.status(HTTP_CODES.CREATED).send(successResponse);
  } catch (error) {
    console.error(error);
    const errorResponse = customResponse({ code: HTTP_CODES.ERROR, message: MESSAGES.ERROR, err: error as object });
    return res.status(HTTP_CODES.ERROR).send(errorResponse);
  }
};
