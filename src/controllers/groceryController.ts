import { Request, Response } from 'express';
import GroceryItem from '../database/model/groceryItem';
import { customResponse } from '../helper/customResponse';
import { HTTP_CODES, MESSAGES } from '../helper/constant';
import { addItemSchema } from '../helper/schema/validator';

export const addItem = async (req: Request, res: Response) => {
  try {
    const { error } = addItemSchema.validate(req.body);
    if (error) {
      const errorResponse = customResponse({
        code: HTTP_CODES.BAD_REQUEST,
        message: error.details[0].message,
        err: error,
      });
      return res.status(HTTP_CODES.BAD_REQUEST).send(errorResponse);
    }
    const { name, price, inventory } = req.body;
    const newGroceryItem = await GroceryItem.create({ name, price, inventory });
    const successResponse = customResponse({
      code: HTTP_CODES.CREATED,
      message: MESSAGES.CREATED,
      data: newGroceryItem,
    });
    return res.status(HTTP_CODES.CREATED).send(successResponse);
  } catch (error) {
    console.error(error);
    const errorResponse = customResponse({ code: HTTP_CODES.ERROR, message: MESSAGES.ERROR, err: error as object });
    return res.status(HTTP_CODES.ERROR).send(errorResponse);
  }
};

export const viewItems = async (req: Request, res: Response) => {
  try {
    const groceryItems = await GroceryItem.findAll();
    const successResponse = customResponse({ code: HTTP_CODES.SUCCESS, message: MESSAGES.FETCHED, data: groceryItems });
    return res.status(200).send(successResponse);
  } catch (error) {
    console.error(error);
    const errorResponse = customResponse({ code: HTTP_CODES.ERROR, message: MESSAGES.ERROR, err: error as object });
    return res.status(HTTP_CODES.ERROR).send(errorResponse);
  }
};

export const removeItem = async (req: Request, res: Response) => {
  try {
    const itemId = parseInt(req.params.itemId, 10);
    const deletedCount = await GroceryItem.destroy({ where: { id: itemId } });
    if (deletedCount === 0) {
      const errorResponse = customResponse({ code: HTTP_CODES.NOT_FOUND, message: MESSAGES.GROCERY_NOT_FOUND });
      return res.status(HTTP_CODES.NOT_FOUND).send(errorResponse);
    } else {
      const successResponse = customResponse({ code: HTTP_CODES.SUCCESS, message: MESSAGES.DELETED });
      return res.status(HTTP_CODES.SUCCESS).send(successResponse);
    }
  } catch (error) {
    console.error(error);
    const errorResponse = customResponse({ code: HTTP_CODES.ERROR, message: MESSAGES.ERROR, err: error as object });

    return res.status(HTTP_CODES.ERROR).send(errorResponse);
  }
};

export const updateItem = async (req: Request, res: Response) => {
  try {
    const itemId = parseInt(req.params.itemId, 10);
    const { name, price, inventory } = req.body;
    const [updatedCount] = await GroceryItem.update({ name, price, inventory }, { where: { id: itemId } });
    if (updatedCount === 0) {
      const errorResponse = customResponse({ code: HTTP_CODES.NOT_FOUND, message: MESSAGES.GROCERY_NOT_FOUND });
      return res.status(HTTP_CODES.NOT_FOUND).send(errorResponse);
    } else {
      const successResponse = customResponse({ code: HTTP_CODES.SUCCESS, message: MESSAGES.GROCERY_UPDATED });
      return res.status(HTTP_CODES.SUCCESS).send(successResponse);
    }
  } catch (error) {
    console.error(error);
    const errorResponse = customResponse({ code: HTTP_CODES.ERROR, message: MESSAGES.ERROR, err: error as object });
    return res.status(HTTP_CODES.ERROR).send(errorResponse);
  }
};

export const manageInventory = async (req: Request, res: Response) => {
  try {
    const itemId = parseInt(req.params.itemId, 10);
    const { inventory } = req.body;
    const [updatedCount] = await GroceryItem.update({ inventory }, { where: { id: itemId } });
    if (updatedCount === 0) {
      const errorResponse = customResponse({ code: HTTP_CODES.NOT_FOUND, message: MESSAGES.GROCERY_NOT_FOUND });
      return res.status(HTTP_CODES.NOT_FOUND).send(errorResponse);
    } else {
      const successResponse = customResponse({ code: HTTP_CODES.SUCCESS, message: MESSAGES.INVENTORY_UPDATED });
      return res.status(HTTP_CODES.SUCCESS).send(successResponse);
    }
  } catch (error) {
    console.error(error);
    const errorResponse = customResponse({ code: HTTP_CODES.ERROR, message: MESSAGES.ERROR, err: error as object });
    return res.status(HTTP_CODES.ERROR).send(errorResponse);
  }
};
