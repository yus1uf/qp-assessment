import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import User from '../database/model/user';
import { customResponse } from '../helper/customResponse';
import { HTTP_CODES, MESSAGES } from '../helper/constant';
import { userSchema } from '../helper/schema/validator';

export const register = async (req: Request, res: Response) => {
  try {
    const { error } = userSchema.validate(req.body);
    if (error) {
      const errorResponse = customResponse({
        code: HTTP_CODES.BAD_REQUEST,
        message: error.details[0].message,
        err: error,
      });
      return res.status(HTTP_CODES.BAD_REQUEST).send(errorResponse);
    }
    const { email, password, name, role } = req.body;
    const existingUser = await User.findOne({ where: { email } });

    if (existingUser) {
      const errorResponse = customResponse({ code: HTTP_CODES.DUPLICATE, message: MESSAGES.DUPLICATE });
      return res.status(HTTP_CODES.DUPLICATE).send(errorResponse);
    }

    const newUser = await User.create({ email, password, name, role });
    const token = jwt.sign(
      { id: newUser.id, username: newUser.email, role: newUser.role, name: newUser.name },
      'QuestionPro Fullstack Node Assessment',
      {
        expiresIn: '1h',
      },
    );
    const successResponse = customResponse({
      code: HTTP_CODES.CREATED,
      message: MESSAGES.REGISTER,
      data: { token: token },
    });
    return res.status(HTTP_CODES.CREATED).send(successResponse);
  } catch (error) {
    console.error(error);
    const errorResponse = customResponse({ code: HTTP_CODES.ERROR, message: MESSAGES.ERROR, err: error as object });
    return res.status(HTTP_CODES.ERROR).send(errorResponse);
  }
};

export const login = async (req: Request, res: Response) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ where: { email } });

    if (!user || !bcrypt.compareSync(password, user.password)) {
      const errorResponse = customResponse({ code: HTTP_CODES.UNAUTHORIZED, message: MESSAGES.INVALID });
      return res.status(HTTP_CODES.UNAUTHORIZED).send(errorResponse);
    }

    const token = jwt.sign(
      { id: user.id, username: user.email, role: user.role, name: user.name },
      'QuestionPro Fullstack Node Assessment',
      {
        expiresIn: '1h',
      },
    );
    const successResponse = customResponse({
      code: HTTP_CODES.SUCCESS,
      message: MESSAGES.LOGIN,
      data: { token: token },
    });
    return res.status(HTTP_CODES.SUCCESS).json(successResponse);
  } catch (error) {
    console.error(error);
    const errorResponse = customResponse({ code: HTTP_CODES.ERROR, message: MESSAGES.ERROR, err: error as object });
    return res.status(HTTP_CODES.ERROR).send(errorResponse);
  }
};
