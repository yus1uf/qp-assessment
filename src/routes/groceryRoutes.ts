import express from 'express';
import isAuthorized from '../middleware/authorizer';
import authenticate from '../middleware/authenticator';
import * as groceryController from '../controllers/groceryController';

const groceryRouter = express.Router();


groceryRouter.post('/addItem', authenticate, isAuthorized(['admin']), groceryController.addItem);
groceryRouter.get('/viewItems', authenticate, isAuthorized(['admin', 'user']), groceryController.viewItems);
groceryRouter.delete('/removeItem/:itemId', authenticate, isAuthorized(['admin']), groceryController.removeItem);
groceryRouter.put('/updateItem/:itemId', authenticate, isAuthorized(['admin']), groceryController.updateItem);
groceryRouter.put('/manageInventory/:itemId', authenticate, isAuthorized(['admin']), groceryController.manageInventory);

export default groceryRouter;
