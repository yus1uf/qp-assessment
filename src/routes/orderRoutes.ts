import express from 'express';
import authenticate from '../middleware/authenticator';
import isAuthorized from '../middleware/authorizer';
import * as orderController from '../controllers/orderController';

const orderRouter = express.Router();

orderRouter.post('/checkout', authenticate, isAuthorized(['user']), orderController.bookItems);

export default orderRouter;
