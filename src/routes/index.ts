import express from 'express';
import userRouter from './userRoutes';
import orderRouter from './orderRoutes';
import groceryRouter from './groceryRoutes';

const route = express();

route.use('/user', userRouter);
route.use('/order', orderRouter);
route.use('/grocery', groceryRouter);

export default route;
