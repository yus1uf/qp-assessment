import 'dotenv/config';
import sequelize from './database/connection/connnection';
import express from 'express';
import bodyParser from 'body-parser';
import route from './routes/index';

const PORT = process.env.APP_PORT || 3000;
const app = express();

app.use(express.json());
app.use(bodyParser.json());
app.use('/api/v1', route);
sequelize.sync().then(() => {
  console.log('Database synchronized.');
  app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
  });
});
