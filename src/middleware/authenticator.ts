import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import User from '../database/model/user';
import { customResponse } from '../helper/customResponse';
import { HTTP_CODES, MESSAGES } from '../helper/constant';

const authenticate = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = req.headers.authorization?.split(' ')[1];

    // Check if the Authorization header is present
    if (!token) {
      const errorResponse = customResponse({ code: HTTP_CODES.UNAUTHORIZED, message: MESSAGES.MISSING_HEADER });
      return res.status(HTTP_CODES.UNAUTHORIZED).send(errorResponse);
    }

    // Verify the JWT token
    const decodedToken: any = jwt.verify(token, 'QuestionPro Fullstack Node Assessment'); // Replace with your secret key

    // Check if the user exists in the database
    const user = await User.findByPk(decodedToken.id);
    if (!user) {
      const errorResponse = customResponse({ code: HTTP_CODES.UNAUTHORIZED, message: MESSAGES.USER_NOT_FOUND });
      return res.status(HTTP_CODES.UNAUTHORIZED).send(errorResponse);
    }

    // Attach the user object to the request for further use in controllers
    req['user'] = {
      id: user.id,
      email: user.email,
      name: user.name,
      role: user.role,
    };

    // Continue to the next middleware or route handler
    next();
  } catch (error) {
    console.error(error);
    const errorResponse = customResponse({ code: HTTP_CODES.UNAUTHORIZED, message: MESSAGES.EXPIRED_TOKEN });
    return res.status(HTTP_CODES.UNAUTHORIZED).send(errorResponse);
  }
};

export default authenticate;
