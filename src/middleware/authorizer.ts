import { Request, Response, NextFunction } from 'express';
import { customResponse } from '../helper/customResponse';
import { HTTP_CODES, MESSAGES } from '../helper/constant';

const isAuthorized = (allowedRoles: string[]) => {
  return (req: Request, res: Response, next: NextFunction) => {
    if (!req.user) {
      const errorResponse = customResponse({ code: HTTP_CODES.UNAUTHORIZED, message: MESSAGES.UNAUTHORIZED });
      return res.status(HTTP_CODES.UNAUTHORIZED).send(errorResponse);
    }

    const userRole = req.user.role;

    if (!userRole || !allowedRoles.includes(userRole)) {
      const errorResponse = customResponse({ code: HTTP_CODES.FORBIDDEN, message: MESSAGES.UNAUTHORIZED });
      return res.status(HTTP_CODES.FORBIDDEN).send(errorResponse);
    }
    next();
  };
};

export default isAuthorized;
