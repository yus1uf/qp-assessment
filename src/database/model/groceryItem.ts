import { DataTypes, Model } from 'sequelize';
import sequelize from '../connection/connnection';

class GroceryItem extends Model {
  public id!: number;
  public name!: string;
  public price!: number;
  public inventory!: number;
}

GroceryItem.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    inventory: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    sequelize,
    modelName: 'GroceryItem',
  },
);

export default GroceryItem;
