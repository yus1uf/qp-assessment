import sequelize from '../connection/connnection';
import { DataTypes, Model } from 'sequelize';
import GroceryItem from './groceryItem';

class Order extends Model {
  public id!: number;
  public userId!: number;
  public totalPrice!: number;
  public readonly groceryItems?: GroceryItem[];
}

Order.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    totalPrice: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
  },
  {
    sequelize,
    modelName: 'Order',
  },
);

Order.belongsToMany(GroceryItem, { through: 'OrderItems' });
GroceryItem.belongsToMany(Order, { through: 'OrderItems' });

export default Order;
