export const HTTP_CODES = {
  SUCCESS: 200,
  CREATED: 201,
  DELETED: 204,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  DUPLICATE: 409,
  ERROR: 500,
};

export const MESSAGES = {
  REGISTER: 'user registered!',
  LOGIN: 'user login successfull!',
  USER_NOT_FOUND: 'user not found!',
  CREATED: 'item created!',
  ERROR: 'internal server error!',
  UNAUTHORIZED: 'unauthorized to perform requested action!',
  GROCERY_NOT_FOUND: 'grocery item not found!',
  DELETED: 'grocery item deleted!',
  GROCERY_UPDATED: 'grocery item updated!',
  INVENTORY_UPDATED: 'inventory updated!',
  FETCHED: 'grocery item fetched!',
  BOOKED: 'order placed!',
  DUPLICATE: 'user already exist!',
  INVALID: 'invalid username or password',
  MISSING_HEADER: 'unauthorized - missing authorization header',
  EXPIRED_TOKEN: 'unauthorized - invalid or expired token',
};

Object.freeze(HTTP_CODES);
Object.freeze(MESSAGES);
