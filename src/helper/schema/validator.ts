// src/validationSchemas/adminController.ts
import Joi from 'joi';

export const addItemSchema = Joi.object({
  name: Joi.string().min(3).required(),
  price: Joi.number().required(),
  inventory: Joi.number().required(),
});

enum UserRole {
  USER = 'user',
  ADMIN = 'admin',
}

export const userSchema = Joi.object({
  email: Joi.string().email().required(),
  name: Joi.string().min(3).required(),
  password: Joi.string().min(5).required(),
  role: Joi.string()
    .valid(...Object.values(UserRole))
    .optional(),
});
