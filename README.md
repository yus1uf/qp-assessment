# qp-assesment

#### Documents Updates

| Version | Create         | Update         |
| :------ | :------------- | :------------- |
| `0.0.1` | `Feb 20, 2024` | `Feb 20, 2024` |

## Environment and Requirements

To run this project, you will need to add have

| Environment | Version                                                                       |
| ----------- | ----------------------------------------------------------------------------- |
| Node.js     | [Node.js](https://nodejs.org/) v20.10.\*                                      |
| npm         | [npm](https://docs.npmjs.com/try-the-latest-stable-version-of-npm) v10.2.\*\* |

**Server:** Node.js(Express)

#### Backend-end tech stack

- Express v4
- TypeScript v5.3.\*

## API Documentation

<span style="color:red">[Click Here](https://documenter.getpostman.com/view/17963881/2sA2r9W3Vh)</span> To see the API documentation

## Run Locally

Clone the project (via HTTP)

```bash
git clone --single-branch -b main https://gitlab.com/yus1uf/qp-assessment.git
```

Clone the project (via SSH)

```bash
git clone --single-branch -b development git@gitlab.com:yus1uf/qp-assessment.git
```

Go to the project directory

```bash
  cd qp-assesment
```

Install dependencies

```bash
npm install
```

Create `.env` file from `.env.sample` in root directory

Start the app Development

```bash
  npm run start
```

#### Project Contributor

| Name                                                    | Email                     |
| :------------------------------------------------------ | :------------------------ |
| [Yusuf Shekh](https://www.linkedin.com/in/yusuf-shekh/) | <khanyusuf9044@gmail.com> |
